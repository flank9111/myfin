<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Filter extends Model
{
    protected $builder;
    protected $request;
    protected $defaults;

    public function __construct($builder, $request, $defaults = [])
    {
        $this->builder = $builder;
        $this->request = $request;
        $this->defaults = $defaults;
    }

    public function apply() {

        foreach (array_merge($this->defaults(),$this->filters()) as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }
        return $this->builder;
    }

    /**
     * @return mixed
     */
    public function filters()
    {
        return $this->request->all();
    }

    public function defaults()
    {
        return collect($this->defaults)->all();
    }
}
