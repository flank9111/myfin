<?php

namespace App\Filters;

use App\House;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OperationFilter extends Filter
{
    public function apply() {

        foreach ($this->filters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }
        return $this->builder;
    }

    public function name($value)
    {
        if (! $value) return;
        $this->builder->where('name', 'like', "%$value%");
    }

    public function bedrooms($value)
    {
        if ($value != 0) {
        $this->builder->where('bedrooms', '=', $value);
        }
    }

    public function bathrooms($value)
    {
        if ($value != 0) {
            $this->builder->where('bathrooms', '=', $value);
        }
    }

    public function garages($value)
    {
        if ($value != 0) {
            $this->builder->where('garages', '=', $value);
        }
    }

    public function storeys($value)
    {
        if ($value != 0) {
            $this->builder->where('storeys', '=', $value);
        }
    }

    public function price_min($value)
    {
        if (! $value) return;
        $this->builder->where('price', '>=', $value);
    }

    public function price_max($value)
    {
        if (! $value) return;
        $this->builder->where('price', '<=', $value);
    }
}
